<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Тестове завдання Агратіна Артур - Завдання 1</title>
    <link rel="stylesheet" href="template/style/style.css">
</head>
<body>
    <section id="page">
        <header>
            <?php require_once('header.php');?>
        </header>
        <div class="clear"></div>
        <!-- Main content-->
        <main>
            <div class="main_title">
                <h1>Soft Group</h1>
                <h2>Агратина Артур Александрович</h2>
            </div>
            <div class="content">

                <h4>Агратина Артур Александрович</h4>
                <p>Дата рождения: 26 июля 1988<br />
                   Телефон: +380937690482<br />
                   E-mail: agratinaa@gmail.com<br >
                   Linkedin: linkedin.com/arturagratina<br />
                   Город проживания: Новоселица<br />
                   Телефон: +380937690482</p>
                <h4>Образование</h4>
                <p>Черновицкий Национальный Университет,<br /> Инженерно-технический факультет,<br /> Информационные сети связи,<br /> Специалист,<br /> Черновцы
                    Высшее, с 09.2005 по 06.2011 (5 лет 9 месяцев)</p>
                <h4>Профессиональные навыки</h4>
                <p>Git - Эксперт, использую в настоящее время.<br/>
                    PhpMyAdmin - Эксперт, использую в настоящее время.<br/>
                    HTML/HTML5 - Средний, использую в настоящее время.<br/>
                    CSS/CSS3 - Средний, использую в настоящее время.<br/>
                    PHP - Средний, использую в настоящее время.<br/>
                    JQuery - Средний, использую в настоящее время.<br/>
                    JavaScript - Начинающий, использую в настоящее время.<br/>
                    Magento - Средний, использую в настоящее время.<br/>
                    Magento2 - Начинающий, использую в настоящее время.<br/>
                    MySql - Средний, использую в настоящее время.<br/>
                    Sql - Средний, использую в настоящее время.</p>
            </div>
        </main>
        <div class="clear"></div>

        <!-- Footer-->
        <footer>
            <?php require_once('footer.php');?>
        </footer>
    </section>
</body>
</html>