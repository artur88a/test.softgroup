<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Тестове завдання Агратіна Артур - Завдання 1</title>
    <link rel="stylesheet" href="template/style/style.css">
</head>
<body>
    <section id="page">
        <header>
            <?php require_once ('header.php');?>
        </header>
        <div class="clear"></div>
        <!-- Left menu-->
        <nav>
            <?php require_once ('leftmenu.php'); ?>
        </nav>
        <!-- Main content-->
        <main>
            <div class="main_title">
                <h1>Soft Group</h1>
                <h2>Тестове завдання</h2>
            </div>
            <div class="content">

                <h5>Завдання 1</h5>
                <p>У форму вводиться число N. Знайти всі досконалі числа до N. Досконале число - це таке число, що дорівнює суммі всіх своїх дільниців крім себе самого. Наприклад,
                    число 6 досконалим, тому що крім себе самого ділиться на числа 1, 2 і 3, які в суммі дають 6.</p>
            </div>
            <div class="forms">
                <form action="?act=do-task1" method="post">
                    <label for="task1">Введіть число N:</label>
                    <input type="number" name="task1">
                    <input type="submit">
                </form>
            </div>
            <div class="result">
                <p>Результат:</p>
                <?php
                    for ($n = 1; $n < $number; $n++){
                        if (is_perfect($n)){
                            echo nl2br($n . PHP_EOL);
                        }
                    }
                ?>
            </div>
            <div class="code">
                <p>Розв'язок:</p>
                <pre>
                <?
                   echo <<<'content'
if ($_POST['task1']){
                        $number = $_POST['task1'];
                        function is_perfect($number) {
                            for ($n = 2; $n <= sqrt($number); $n++) {
                                if (!($number % $n)) {
                                    $d += $n;
                                    if ($n <> $number / $n){
                                        $d += $number / $n;
                                    }
                                }
                            }
                            return ++$d == $number;
                        }
                       for ($n = 1; $n < $number; $n++){
                            if (is_perfect($n)){
                                echo nl2br($n . PHP_EOL);
                            }
                        }
                    }
content;
                ?>
                    </pre>
            </div>
        </main>
        <div class="clear"></div>

        <!-- Footer-->
        <footer>
            <?php require_once ('footer.php');?>
        </footer>
    </section>
</body>
</html>