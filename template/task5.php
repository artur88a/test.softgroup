<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Тестове завдання Агратіна Артур - Завдання 1</title>
    <link rel="stylesheet" href="template/style/style.css">
</head>
<body>
<section id="page">
    <header>
        <?php require_once ('header.php');?>
    </header>
    <div class="clear"></div>
    <!-- Left menu-->
    <nav>
        <?php require_once ('leftmenu.php'); ?>
    </nav>
    <!-- Main content-->
    <main>-
        <div class="main_title">
            <h1>Soft Group</h1>
            <h2>Тестове завдання</h2>
        </div>
        <div class="content">
            <h5>Завдання 5</h5>
            <p>Задано текст. Словом вважається послідовність непробільних символів, які йдуть підряд. Слова розділені одним або більшим числом пробілів або символами
                кінця рядка.</p>
            <p>Для кожного слова з цього тексту підрахуйте, скільки разів воно зустрічалося в цьому тексті раніше. Використайте словники.</p>
            <p>Приклад:</p>
            <div>


                <div class="example">Ввід:</div>
                <div class="example">Вивід:</div>
                <div style="clear: left"></div>
                <div class="example">one two one two three</div>
                <div class="example">0 0 1 1 0</div>
                <div style="clear: left"></div>
                <div class="example">She sells sea shells on the sea shore; The shells that she sells are sea shells I'm sure.
                    So if she sells sea shells on the sea shore, I'm sure that the shells are sea shore shells.</div>
                <div class="example">0 0 0 0 0 0 1 0 0 1 0 0 1 0 2 2 0 0 0 0 1 2 3 3
                    1 1 4 0 1 0 1 2 4 1 5 0 0</div>
                <div style="clear: left"></div>
            </div><br/>
        </div>
        <div class="forms">
            <form action="?act=do-task5" method="post">
                <label for="task5">Введіть ваш текст:</label>
                <input type="text" name="task5">
                <input type="submit">
            </form>
        </div>
        <div class="result">
            <p>Результат:</p>
            <?php if(isset($num)):?>
                <?php foreach ($num as $item):?>
                    <?php echo $item;?>
                <?php endforeach;?>
            <?php endif;?>
        </div>
        <div class="code">
            <p>Розв'язок:</p>
            <pre>
            <?
            echo <<<'content'
function multiexplode ($delimiters,$string) {
                $ready = str_replace($delimiters, $delimiters[0], $string);
                $launch = explode($delimiters[0], $ready);
                return  $launch;
            }
            $num = array();
            if (isset($_POST['task5'])){
                $number = htmlspecialchars($_POST['task5']);
                $numbers = multiexplode(array(" ",",",".",";"),$number);
            
                $count = count($numbers);
                    for ($i=0; $i<$count; $i++){
                        $word = $numbers[$i];
                        if ($i==0){
                            $num[] = 0;
                        }
                        $str = strlen($word);
                        if (!$str == null) {
                            $counter = '0';
                            for ($j = 0; $j < $i; $j++) {
                                if ($word == $numbers[$j]) {
                                    ++$counter;
                                }
                            }
                            if (!$j < $i && !$counter == 0) {
                                $num[] = $counter;
                            } elseif ($j = $i && $counter == 0) {
                                $num[] = 0;
                            }
                        }
                    }
            }
content;
            ?>
            </pre>
        </div>
        </main>
        <div class="clear"></div>
        <!-- Footer-->
        <footer>
            <?php require_once ('footer.php');?>
        </footer>
    </section>
</body>
</html>