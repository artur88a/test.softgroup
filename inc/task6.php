<?php
if (isset($_POST['name']) && isset($_POST['value'])){
    $values = ($_POST['value']);
    arsort($values);
    $values = array_flip($values);
    $names = $_POST['name'];
    $result = array();
    foreach ($values as $k => $v){
        if(!isset($names[$v])){
            continue;
        }

        $result[] = array(
            'name' => $names[$v],
            'value' => $k
        );
        if(count($result) == 3){
            break;
        }
    }
}
?>

