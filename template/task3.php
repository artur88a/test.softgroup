<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Тестове завдання Агратіна Артур - Завдання 1</title>
    <link rel="stylesheet" href="template/style/style.css">
</head>
<body>
<section id="page">
    <header>
        <?php require_once ('header.php');?>
    </header>
    <div class="clear"></div>
    <!-- Left menu-->
    <nav>
        <?php require_once ('leftmenu.php'); ?>
    </nav>
    <!-- Main content-->
    <main>
        <div class="main_title">
            <h1>Soft Group</h1>
            <h2>Тестове завдання</h2>
        </div>
            <div class="content">
                <h5>Завдання 3</h5>
                <p>Вводяться N натуральних чисел більше 2. Підрахувати, скільки серед них простих чисел.</p>
            </div>
        <div class="forms">
            <form action="?act=do-task3" method="post">
                <label for="task3">Введіть N натуральних чисел(більше 2):</label>
                <input type="text" name="task3" placeholder="2, 56, 85, 42, 11">
                <input type="submit">
            </form>
        </div>
        <div class="result">

            <?php
            if ($_POST['task3']) {
                if ($number == null) {
                    echo "<p>Результат:</p>";
                    echo "<p>Всі числа не є простими!</p>";
                }elseif (empty($number)){
                    echo "<p>Введіть числа!</p>";
                } else {
                    echo "<p>Результат:</p>";
                    echo "<p>" . $number . "</p>";
                }
            }
            ?>
        </div>
        <div class="code">
            <pre>
            <?php if (!$number == null) {
            echo "<p>Розв'язок:</p>";

            echo <<<'content'
                if ($_POST['task3']){
                    $numbers = $_POST['task3'];
                    function if_natural($n){
                        if($n==1 or $n==0)
                        {
                            return false;
                        }
                        for($d=2; $d*$d<=$n; $d++)
                        {
                            if($n%$d==0)return false;
                        }
                        return true;
                    }
                    $array = explode(', ', $numbers);
                    foreach ($array as $key => $value)
                    {
                        if(!if_natural($value))
                        {
                            unset($array["$key"]);
                        }
                    }
                    $number = implode(', ', $array);
                }
content;
            }
            ?>
                </pre>
        </div>
        </main>
        <div class="clear"></div>
        <!-- Footer-->
        <footer>
            <?php require_once ('footer.php');?>
        </footer>
    </section>
</body>
</html>