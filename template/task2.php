<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Тестове завдання Агратіна Артур - Завдання 1</title>
    <link rel="stylesheet" href="template/style/style.css">
</head>
<body>
    <section id="page">
        <header>
            <?php require_once ('header.php');?>
        </header>
        <div class="clear"></div>
        <!-- Left menu-->
        <nav>
            <?php require_once ('leftmenu.php'); ?>
        </nav>
        <!-- Main content-->
        <main>
            <div class="main_title">
                <h1>Soft Group</h1>
                <h2>Тестове завдання</h2>
            </div>
            <div class="content">
                <h5>Завдання 2</h5>
                <p>У форму вводиться текст, слова в якому розділенні пробілами і розділовими знаками. Вилучити з цього тексту всі слова найбільшої довжини.
                    (Слів може бути декілька).</p>
            </div>
            <div class="forms">
                <form action="?act=do-task2" method="post">
                    <label for="task2">Введіть ваш текст:</label>
                    <input type="text" name="task2">
                    <input type="submit">
                </form>
            </div>
            <div class="result">
                <p>Результат:</p>
                <?php
                if ($_POST) {
                    foreach ($str as $item) {
                        if (strlen($item) == $max) {
                            echo $item . "<br>";
                        }
                    }
                    echo "<br>";
                }
                ?>
            </div>
            <div class="code">
                <p>Розв'язок:</p>
                <pre>
                <?
                echo <<<'content'
function multiexplode ($delimiters,$string) {

                    $ready = str_replace($delimiters, $delimiters[0], $string);
                    $launch = explode($delimiters[0], $ready);
                    return  $launch;
                }
                if ($_POST['task2']){
                    $string = $_POST['task2'];
                
                    $str = multiexplode(array(" ",",",".","!","?"),$string);
                    $max = 0;
                    $countStr = count($str);
                    foreach ($str as $items)
                    {
                        if ($max < strlen($items)){
                                $max = strlen($items);
                        }
                    }
                }
                
                foreach ($str as $item){
                    if (strlen($item)== $max){
                        echo $item.", ";
                    }
                }
content;
            ?>
                    </pre>
        </div>
        </main>
        <div class="clear"></div>
        <!-- Footer-->
        <footer>
            <?php require_once ('footer.php');?>
        </footer>
    </section>
</body>
</html>