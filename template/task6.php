<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Тестове завдання Агратіна Артур - Завдання 1</title>
    <link rel="stylesheet" href="template/style/style.css">
</head>
<body>
<section id="page">
    <header>
        <?php require_once ('header.php');?>
    </header>
    <div class="clear"></div>
    <!-- Left menu-->
    <nav>
        <?php require_once ('leftmenu.php'); ?>
    </nav>
    <!-- Main content-->
    <main>
        <div class="main_title">
            <h1>Soft Group</h1>
            <h2>Тестове завдання</h2>
        </div>
            <div class="content">
                <h5>Завдання 6</h5>
                <p>Змагання з гри «Тетріс-онлайн» проводяться за такими правилами:</p>
                <p>Кожен учасник реєструється на сайті гри під певним ігровим ім'ям. Імена учасників не повторюються.</p>
                <p>Чемпіонат проводиться протягом певного часу. У будь-який момент цього часу будь-який зареєстрований учасник може зайти на сайт чемпіонату і почати залікову гру.
                    Після закінчення гри її результат (кількість балів) фіксується і заноситься до протоколу.</p>
                <p>Учасники мають право грати кілька разів. Кількість спроб одного учасника не обмежується. Остаточний результат учасника визначається по одній грі,
                    кращою для даного учасника. Більш високе місце в змаганнях займає учасник, що показав кращий результат. При рівності результатів більш високе місце
                    займає учасник, раніше показав кращий результат.</p>
                <p>В ході змагань заповнюється протокол, кожен рядок якого описує одну гру і містить результат учасника і його ігрове ім'я. Протокол формується в
                    реальному часі по ходу проведення чемпіонату, тому рядки в ньому розташовані в порядку проведення ігор: чим раніше зустрічається рядок у протоколі,
                    тим раніше закінчилася відповідна цьому рядку гра.</p>
                <p>Напишіть програму, яка за даними протоколу визначає переможця і призерів. Гарантується, що в чемпіонаті бере участь не менше трьох гравців.</p>
                <p>Приклад:</p>
                <div style="float: left; width: 300px;">
                    <div>Ввід:</div>
                    <div>9</div>
                    <div>69485 Jack</div>
                    <div>95715 qwerty</div>
                    <div>95715 Alex</div>
                    <div>83647 M</div>
                    <div>197128 qwerty</div>
                    <div>95715 Jack</div>
                    <div>93289 Alex</div>
                    <div>95715 Alex</div>
                    <div>95715 M</div>
                </div>
                <div style="float: left; width: 300px;">
                    <div>Вивід:</div>
                    <div>1 місце. Qwerty (197128)</div>
                    <div>2 місце. Alex (95715)</div>
                    <div>3 місце. Jack (95715)</div>
                </div>
            </div>
        <div style="clear: left"></div>
        <div class="forms">
            <?php
            $numberOf = isset($_POST['number-task6'])?$_POST['number-task6']:false;
            ?>
            <form action="<?php echo $numberOf?'?act=do-task6':''?>" name="game" method="post">
                <?php if(!$numberOf):?>
                    <label for="number-task6">Введіть кільекість учасників:</label>
                    <input type="number" name="number-task6"><br/>
                <?php else:?>
                    <?for ($i=1;$i<=$numberOf;$i++):?>
                        <label for="name<?php echo $i;?>-task6">Введіть ім'я <?php echo $i?>-го учасника:</label>
                        <input type="text" name="name[]">
                        <input type="number" name="value[]"><br/>
                    <?endfor;?>
                <?php endif;?>
                <input type="submit">
            </form>
        </div>
        <div class="result">
            <p>Результат:</p>
            <?php if(isset($result)):$i=0;?>
                <?php foreach ($result as $player):$i++;?>
                    <?php echo "$i місце. ".$player['name']; echo "(".$player['value'].")<br/>";?>
                <?php endforeach;?>
            <?php endif;?>
        </div>
        <div class="code">
            <p>Розв'язок:</p>
            <pre>
            <?
            echo <<<'content'
if (isset($_POST['name']) && isset($_POST['value'])){
                $values = ($_POST['value']);
                arsort($values);
                $values = array_flip($values);
                $names = $_POST['name'];
                $result = array();
                foreach ($values as $k => $v){
                    if(!isset($names[$v])){
                        continue;
                    }
            
                    $result[] = array(
                        'name' => $names[$v],
                        'value' => $k
                    );
                    if(count($result) == 3){
                        break;
                    }
                }
            }
content;
            ?>
            </pre>
        </div>
        </main>
        <div class="clear"></div>
        <!-- Footer-->
        <footer>
            <?php require_once ('footer.php');?>
        </footer>
    </section>
</body>
</html>